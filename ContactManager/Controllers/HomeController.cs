﻿using ContactManager.Models;
using System;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ContactManager.Controllers
{
    public class HomeController : Controller
    {

        // GET: Contact
        public ActionResult Index()
        {
            return View();
        }

        // GET: All Contacts
        public JsonResult GetAllContacts()
        {
            using (ContactDBContext db = new ContactDBContext())
            {
                var contactList = db.contacts
                    .Include(c => c.PhoneNumbers)
                    .Include(c => c.Emails)
                    .Include(c => c.Tags)
                    .Select(c => new {
                        c.ID,
                        c.FirstName,
                        c.LastName,
                        c.Address,
                        c.City,
                        c.ZipCode,
                        PhoneNumbers = c.PhoneNumbers.Select(p => p.Number),
                        Emails = c.Emails.Select(e => e.Address),
                        Tags = c.Tags.Select(t => t.Name)
                    }).ToList();

                return Json(contactList, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult SearchByCriteria(string query, string criteria)
        {
            using (ContactDBContext db = new ContactDBContext())
            {
                IEnumerable<Contact> contacts = null;
                if (criteria == "tag")
                {
                    var tags = db.tags
                        .Include(t => t.Contacts)
                        .Where(t => t.Name == query);
                    if (tags.Any())
                        contacts = tags.First().Contacts;
                    else contacts = new List<Contact>();

                } else
                {
                    contacts = db.contacts
                        .Include(c => c.PhoneNumbers)
                        .Include(c => c.Emails)
                        .Include(c => c.Tags);
                    if (criteria == "firstName")
                    {
                        contacts = contacts.Where(c => c.FirstName.Contains(query));

                    } else if (criteria == "lastName")
                    {
                        contacts = contacts.Where(c => c.LastName.Contains(query));
                    }
                }

                var contactList = contacts
                    .Select(c => new {
                        c.ID,
                        c.FirstName,
                        c.LastName,
                        c.Address,
                        c.City,
                        c.ZipCode,
                        PhoneNumbers = c.PhoneNumbers.Select(p => p.Number),
                        Emails = c.Emails.Select(e => e.Address),
                        Tags = c.Tags.Select(t => t.Name)
                    }).ToList();

                return Json(contactList, JsonRequestBehavior.AllowGet);
            }
        }

        //GET: Contact by Id
        public JsonResult GetContactById(string id)
        {
            using (ContactDBContext db = new ContactDBContext())
            {
                // TODO contactviewmodel
                var contactId = Convert.ToInt32(id);
                var contact = db.contacts.Find(contactId);
                ContactViewModel contactModel = new ContactViewModel()
                {
                    FirstName = contact.FirstName,
                    LastName = contact.LastName,
                    Address = contact.Address,
                    City = contact.City,
                    ZipCode = contact.ZipCode,
                    PhoneNumbers = contact.PhoneNumbers.Select(p => p.Number),
                    Emails = contact.Emails.Select(e => e.Address),
                    Tags = contact.Tags.Select(t => t.Name)
                };
                return Json(contactModel, JsonRequestBehavior.AllowGet);
            }
        }

        //Update Contact
        public string UpdateContact(ContactViewModel model)
        {
            if (model != null)
            {
                using (ContactDBContext db = new ContactDBContext())
                {
                    int contactId = Convert.ToInt32(model.ID);
                    Contact contact = db.contacts.Find(contactId);
                    contact.FirstName = model.FirstName;
                    contact.LastName = model.LastName;
                    contact.Address = model.Address;
                    contact.City = model.City;
                    contact.ZipCode = model.ZipCode;

                    // removed tags
                    if (contact.Tags != null && contact.Tags.Count > 0)
                    {
                        List<Tag> tags = new List<Tag>(contact.Tags);
                        foreach (Tag tag in tags)
                        {
                            if (model.Tags == null || !model.Tags.Contains(tag.Name))
                            {
                                db.tags.Remove(tag);
                            }
                        }
                    }

                    // add new tags
                    if (model.Tags != null && model.Tags.Count() > 0)
                    {
                        List<Tag> tags = db.tags.ToList();
                        foreach (string text in model.Tags)
                        {
                            var tag = tags.Find(t => t.Name == text);
                            // if tag does not exist add it
                            if (tag == null)
                            {
                                tag = new Tag { Name = text };
                                db.tags.Add(tag);
                                contact.Tags.Add(tag);
                            }
                            // if contact was not tagged with tag
                            else if (!contact.Tags.Contains(tag))
                            {
                                contact.Tags.Add(tag);
                            }
                        }
                    }

                    // removed phone numbers
                    List<PhoneNumber> phoneNumbers = new List<PhoneNumber>(contact.PhoneNumbers);
                    foreach (PhoneNumber phone in phoneNumbers)
                    {
                        if (!model.PhoneNumbers.Contains(phone.Number))
                        {
                            db.phoneNumbers.Remove(phone);
                        }
                    }

                    // add new phone numbers
                    phoneNumbers = db.phoneNumbers.ToList();
                    foreach (string number in model.PhoneNumbers)
                    {
                        if (phoneNumbers.Find(p => p.Number == number) == null)
                        {
                            var phone = new PhoneNumber
                            {
                                ContactId = contact.ID,
                                Number = number
                            };
                            db.phoneNumbers.Add(phone);
                        }
                    }

                    // removed emails
                    List<Email> emails = new List<Email>(contact.Emails);
                    foreach (Email email in emails)
                    {
                        if (!model.Emails.Contains(email.Address))
                        {
                            db.emails.Remove(email);
                        }
                    }

                    // add new emails
                    emails = db.emails.ToList();
                    foreach (string address in model.Emails)
                    {
                        if (emails.Find(e => e.Address == address) == null)
                        {
                            var email = new Email
                            {
                                ContactId = contact.ID,
                                Address = address
                            };
                            db.emails.Add(email);
                        }
                    }

                    db.SaveChanges();
                    return "Contact record updated successfully";
                }
            }
            else
            {
                return "Invalid contact record";
            }
        }

        // Add contact
        public string AddContact(ContactViewModel model)
        {
            if (model != null)
            {
                using (ContactDBContext db = new ContactDBContext())
                {
                    var contact = new Contact
                    {
                        FirstName = model.FirstName,
                        LastName = model.LastName,
                        Address = model.Address,
                        City = model.City,
                        ZipCode = model.ZipCode,
                        Tags = new List<Tag>()
                    };
                    db.contacts.Add(contact);

                    if (model.Tags != null && model.Tags.Count() > 0)
                    {
                        List<Tag> tags = db.tags.ToList();
                        foreach (string text in model.Tags)
                        {
                            Tag tag = tags.Find(t => t.Name == text);
                            if (tag == null)
                            {
                                tag = new Tag { Name = text };
                                db.tags.Add(tag);
                            }
                            contact.Tags.Add(tag);
                        };
                    }
                    
                    foreach (string number in model.PhoneNumbers)
                    {
                        var phone = new PhoneNumber
                        {
                            ContactId = contact.ID,
                            Number = number
                        };
                        db.phoneNumbers.Add(phone);
                    }

                    foreach (string address in model.Emails)
                    {
                        var email = new Email
                        {
                            ContactId = contact.ID,
                            Address = address
                        };
                        db.emails.Add(email);
                    }

                    db.SaveChanges();
                    return "Contact record added successfully";
                }
            }
            else
            {
                return "Invalid contact record";
            }
        }

        // Delete contact
        public string DeleteContact(string contactId)
        {

            if (!String.IsNullOrEmpty(contactId))
            {
                try
                {
                    int _contactId = Int32.Parse(contactId);
                    using (ContactDBContext db = new ContactDBContext())
                    {
                        var contact = db.contacts.Find(_contactId);
                        db.contacts.Remove(contact);
                        db.SaveChanges();
                        return "Selected contact record deleted sucessfully";
                    }
                }
                catch (Exception)
                {
                    return "Contact details not found";
                }
            }
            else
            {
                return "Invalid operation";
            }
        }
    }
}