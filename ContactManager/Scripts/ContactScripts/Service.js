﻿app.service("crudAJService", function ($http) {

    // get all Contacts
    this.getContacts = function () {
        return $http.get("Home/GetAllContacts");
    };

    // search contacts by criteria
    this.searchContacts = function (query, criteria) {
        return $http.get("Home/SearchByCriteria", {
            params: {
                query: query,
                criteria: criteria
            }
        });
    };

    // get Contact by ContactId
    this.getContact = function (contactId) {
        return $http.get("Home/GetContactById/" + contactId);
    }

    // update Contact
    this.updateContact = function (contact) {
        return $http.post("Home/UpdateContact", JSON.stringify(contact));
    }

    // add Contact
    this.AddContact = function (contact) {
        return $http.post("Home/AddContact", JSON.stringify(contact));
    }

    // delete Contact
    this.DeleteContact = function (contactId) {
        return $http({
            url: "Home/DeleteContact/",
            method: "POST",
            params: {
                contactId: contactId
            }
        })
    }
});