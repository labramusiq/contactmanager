﻿app.controller("ContactsController", ['$scope', 'crudAJService', '$location', function ($scope, crudAJService, $location) {

    $scope.showDetails = function(id) {
        $location.path('/details/' + id);
    }

    $scope.showAdd = function() {
        $location.path('/add');
    }

    $scope.showEdit = function(id) {
        $location.path('/edit/' + id);
    }

    //To Get all contact records  
    function GetAllContacts() {
        //debugger;
        var getContactData = crudAJService.getContacts();
        getContactData.then(function (contact) {
            $scope.contacts = contact.data;
        }, function () {
            alert('Error in getting contact records');
        });
    }

    $scope.deleteContact = function (contact) {
        var getContactData = crudAJService.DeleteContact(contact.ID);
        getContactData.then(function (msg) {
            alert(msg.data);
            GetAllContacts();
        }, function () {
            alert('Error in deleting contact record');
        });
    }

    GetAllContacts();

    $scope.Search = function () {
        var getContactData = crudAJService.searchContacts($scope.searchQuery, $scope.searchCriteria);
        getContactData.then(function (contact) {
            $scope.contacts = contact.data;
        }, function () {
            alert('Error in getting contact records');
        });
    }
    
    $scope.searchCriteria = 'firstName';
}]);