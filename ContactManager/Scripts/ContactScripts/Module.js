﻿var app = angular.module("ContactManager", ['ngRoute', 'ngTagsInput', 'ui.bootstrap']);

var configFunction = function ($routeProvider, $locationProvider) {
    $routeProvider.
        when('/', {
            controller: 'ContactsController',
            templateUrl: '/Pages/contacts.html'
        })
        .when('/details/:id', {
            controller: 'ContactEditController',
            templateUrl: '/Pages/details.html'
        })
        .when('/add', {
            controller: 'ContactEditController',
            templateUrl: '/Pages/add.html'
        })
        .when('/edit/:id', {
            controller: 'ContactEditController',
            templateUrl: '/Pages/edit.html'
        })
        .otherwise({
            redirectTo: '/'      
        });

    $locationProvider.hashPrefix('').html5Mode([true, false, true]);
}
configFunction.$inject = ['$routeProvider', '$locationProvider'];

app.config(configFunction);