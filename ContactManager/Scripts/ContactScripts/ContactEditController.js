﻿app.controller("ContactEditController", ['$scope', '$routeParams', 'crudAJService', '$location', function ($scope, $routeParams, crudAJService, $location) {

    $scope.Cancel = function () {
        $location.path('/');
    };

    $scope.addNewPhones = function () {
        var newPhone = { number: '' };
        $scope.contactPhoneNumbers.push(newPhone);
    }

    $scope.removePhone = function (index) {
        $scope.contactPhoneNumbers.splice(index, 1);
    }

    $scope.addNewEmails = function () {
        var newEmail = { address: '' };
        $scope.contactEmails.push(newEmail);
    }

    $scope.removeEmail = function (index) {
        $scope.contactEmails.splice(index, 1);
    }

    $scope.AddContact = function () {
        var phones = [];
        for (i = 0; i < $scope.contactPhoneNumbers.length; ++i) {
            phones.push($scope.contactPhoneNumbers[i].number);
        }
        var emails = [];
        for (i = 0; i < $scope.contactEmails.length; ++i) {
            emails.push($scope.contactEmails[i].address);
        }
        var tags = [];
        for (i = 0; i < $scope.contactTags.length; ++i) {
            tags.push($scope.contactTags[i].text);
        }

        var Contact = {
            FirstName: $scope.contactFirstName,
            LastName: $scope.contactLastName,
            Address: $scope.contactAddress,
            City: $scope.contactCity,
            ZipCode: $scope.contactZipCode,
            PhoneNumbers: phones,
            Emails: emails,
            Tags: tags
        };

        var getContactData = crudAJService.AddContact(Contact);
        getContactData.then(function (msg) {
            $location.path('/');
            alert(msg.data);
        }, function () {
            alert('Error in adding contact record');
        });
    }

    $scope.UpdateContact = function () {
        var phones = [];
        for (i = 0; i < $scope.contactPhoneNumbers.length; ++i) {
            phones.push($scope.contactPhoneNumbers[i].number);
        }
        var emails = [];
        for (i = 0; i < $scope.contactEmails.length; ++i) {
            emails.push($scope.contactEmails[i].address);
        }
        var tags = [];
        for (i = 0; i < $scope.contactTags.length; ++i) {
            tags.push($scope.contactTags[i].text);
        }

        var Contact = {
            ID: $scope.contactId,
            FirstName: $scope.contactFirstName,
            LastName: $scope.contactLastName,
            Address: $scope.contactAddress,
            City: $scope.contactCity,
            ZipCode: $scope.contactZipCode,
            PhoneNumbers: phones,
            Emails: emails,
            Tags: tags
        };

        var getContactData = crudAJService.updateContact(Contact);
        getContactData.then(function (msg) {
            $location.path('/');
            alert(msg.data);
        }, function () {
            alert('Error in updating contact record');
        });
    }

    $scope.GetContact = function() {
        var contactID = $routeParams.id;
        var getContactData = crudAJService.getContact(contactID);
        getContactData.then(function (_contact) {
            var contact = _contact.data;
            $scope.contactFirstName = contact.FirstName;
            $scope.contactLastName = contact.LastName;
            $scope.contactAddress = contact.Address;
            $scope.contactCity = contact.City;
            $scope.contactZipCode = contact.ZipCode;
            $scope.contactPhoneNumbers = contact.PhoneNumbers;
            $scope.contactEmails = contact.Emails;
            $scope.contactTags = contact.Tags;

        }, function () {
            alert('Error in getting contact records');
        });
    }

    $scope.EditContact = function () {
        var contactID = $routeParams.id;
        var getContactData = crudAJService.getContact(contactID);
        getContactData.then(function (_contact) {
            var contact = _contact.data;
            var phones = [];
            for (i = 0; i < contact.PhoneNumbers.length; ++i) {
                var phone = { number: contact.PhoneNumbers[i] };
                phones.push(phone);
            }
            var emails = [];
            for (i = 0; i < contact.Emails.length; ++i) {
                var email = { address: contact.Emails[i] };
                emails.push(email);
            }
            var tags = [];
            for (i = 0; i < contact.Tags.length; ++i) {
                var tag = { text: contact.Tags[i] };
                tags.push(tag);
            }

            $scope.contactId = $routeParams.id;
            $scope.contactFirstName = contact.FirstName;
            $scope.contactLastName = contact.LastName;
            $scope.contactAddress = contact.Address;
            $scope.contactCity = contact.City;
            $scope.contactZipCode = contact.ZipCode;

            $scope.contactPhoneNumbers = phones;
            $scope.contactEmails = emails;
            $scope.contactTags = tags;

        }, function () {
            alert('Error in getting contact records');
        });
    }

    function ClearFields() {
        $scope.contactFirstName = "";
        $scope.contactLastName = "";
        $scope.contactAddress = "";
        $scope.contactCity = "";
        $scope.contactZipCode = "";
        $scope.contactPhoneNumbers = [];
        $scope.addNewPhones();
        $scope.contactEmails = [];
        $scope.addNewEmails();
        $scope.contactTags = [];
    }

    ClearFields();
}]);