namespace ContactManager.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using ContactManager.Models;
    using System.Collections.Generic;

    internal sealed class Configuration : DbMigrationsConfiguration<ContactDBContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(ContactDBContext context)
        {
            var tags = new List<Tag>
            {
                new Tag { ID = 1, Name = "business" },
                new Tag { ID = 2, Name = "personal" },
                new Tag { ID = 3, Name = "family" },
                new Tag { ID = 4, Name = "marketing" },
                new Tag { ID = 5, Name = "developer" },
                new Tag { ID = 6, Name = "manager" },
            };
            tags.ForEach(t => context.tags.AddOrUpdate(tag => tag.ID, t));
            context.SaveChanges();

            var contact1 = new Contact { ID = 1, FirstName = "Ivan", LastName = "Horvat", Address = "Strojarska 2", City = "Zagreb",
                 ZipCode = 10000, Tags = new List<Tag>()  };
            var contact2 = new Contact { ID = 2, FirstName = "Luka", LastName = "Pavic", Address = "Unska 3", City = "Zagreb",
                 ZipCode = 10000, Tags = new List<Tag>()  };
            var contact3 = new Contact { ID = 3, FirstName = "Ana", LastName = "Kovac", Address = "Unska 4", City = "Zagreb",
                 ZipCode = 10000, Tags = new List<Tag>()  };
            var contact4 = new Contact { ID = 4, FirstName = "Marko", LastName = "Ivankovic", Address = "Masarykova", City = "Budimpesta",
                 ZipCode = 1051, Tags = new List<Tag>()  };
            var contact5 = new Contact { ID = 5, FirstName = "John", LastName = "Smith", Address = "Park Avenue", City = "New York",
                 ZipCode = 10001, Tags = new List<Tag>()  };

            contact1.Tags.Add(tags.Find(t => t.ID == 1));
            contact1.Tags.Add(tags.Find(t => t.ID == 4));

            contact2.Tags.Add(tags.Find(t => t.ID == 2));
            contact2.Tags.Add(tags.Find(t => t.ID == 3));

            contact3.Tags.Add(tags.Find(t => t.ID == 2));

            contact4.Tags.Add(tags.Find(t => t.ID == 1));
            contact4.Tags.Add(tags.Find(t => t.ID == 5));

            contact5.Tags.Add(tags.Find(t => t.ID == 1));
            contact5.Tags.Add(tags.Find(t => t.ID == 6));

            context.contacts.AddOrUpdate(contact => contact.ID, contact1);
            context.contacts.AddOrUpdate(contact => contact.ID, contact2);
            context.contacts.AddOrUpdate(contact => contact.ID, contact3);
            context.contacts.AddOrUpdate(contact => contact.ID, contact4);
            context.contacts.AddOrUpdate(contact => contact.ID, contact5);

            context.SaveChanges();

            var phones = new List<PhoneNumber>
            {
                new PhoneNumber { ID = 1, ContactId = 1, Number = "385 97 1111 111"  },
                new PhoneNumber { ID = 2, ContactId = 2, Number = "385 97 1111 222"  },
                new PhoneNumber { ID = 3, ContactId = 3, Number = "385 97 1111 333"  },
                new PhoneNumber { ID = 4, ContactId = 3, Number = "385 97 2222 333"  },
                new PhoneNumber { ID = 5, ContactId = 4, Number = "385 97 1111 444"  },
                new PhoneNumber { ID = 6, ContactId = 5, Number = "385 97 1111 555"  }
            };
            phones.ForEach(p => context.phoneNumbers.AddOrUpdate(phone => phone.ID, p));
            context.SaveChanges();

            var emails = new List<Email>
            {
                new Email { ID = 1, ContactId = 1, Address = "ivan@yahoo.com" },
                new Email { ID = 2, ContactId = 2, Address = "luka@yahoo.com" },
                new Email { ID = 3, ContactId = 3, Address = "ana@yahoo.com" },
                new Email { ID = 4, ContactId = 4, Address = "marko@yahoo.com" },
                new Email { ID = 5, ContactId = 4, Address = "marko@gmail.com" },
                new Email { ID = 6, ContactId = 5, Address = "john@yahoo.com" }
            };
            emails.ForEach(e => context.emails.AddOrUpdate(email => email.ID, e));
            context.SaveChanges();
        }
    }
}
