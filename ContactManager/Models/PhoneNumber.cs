﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ContactManager.Models
{
    public class PhoneNumber
    {
        public int ID { get; set; }

        [Required]
        public int ContactId { get; set; }

        [Required]
        [Phone]
        [StringLength(20)]
        public string Number { get; set; }

        public virtual Contact Contact { get; set; }
    }
}