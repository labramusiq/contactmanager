﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ContactManager.Models
{
    public class Email
    {
        public int ID { get; set; }

        [Required]
        public int ContactId { get; set; }

        [Required]
        [EmailAddress]
        [StringLength(100)]
        public string Address { get; set; }

        public virtual Contact Contact { get; set; }
    }
}