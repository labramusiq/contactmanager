﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ContactManager.Models
{
    public partial class ContactDBContext : DbContext
    {
        public ContactDBContext()
            : base("name=ContactDBContext")
        {
        }

        public virtual DbSet<Contact> contacts { get; set; }
        public virtual DbSet<Email> emails { get; set; }
        public virtual DbSet<PhoneNumber> phoneNumbers { get; set; }
        public virtual DbSet<Tag> tags { get; set; }
    }
}